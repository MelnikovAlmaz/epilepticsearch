$('#epileptic-search-upload').click(function () {
    if (!$('#file-input').val()) {
        showModal('#error-modal', 'Выберите хотя бы один файл');
        return
    }
    let form = $('form');
    let formData = new FormData(form[0]);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            document.getElementById('epileptic-results').innerHTML = '';
            resultCallback(data)
        },
        error: function (data) {
            console.log(data);
        }
    })
});

function resultCallback(data) {
    let error_code = data.error_code;
    let text;
    if (data.success === false) {
        if (error_code === 'NO_FILES_PRESENT') {
            text = 'Не выбрано ни одного файла';
        }
        showModal('#error-modal', text)
    } else {
        showResults(data.results, data.report_url);
    }
}

function showModal(modalSelector, text) {
    let modal = $(modalSelector);
    let modal_body = $('.modal-body p');
    modal_body.text(text);
    modal.modal('show');
}

function showResults(results, download_link) {
    console.log(results);
    console.log(download_link);
    let resultsWrapper = $('#epileptic-results');
    let downloadLink = $('#download-link');
    downloadLink.attr("href", download_link);

    resultsWrapper.css({'display': 'flex'});

    for (let i = 0; i < results.length; i++) {
        let probs = '';
        for (let prob in results[i].types) {
            probs +=
                '<div class="row card-text">' +
                '<p class="col-6">' +
                results[i].types[prob].name +
                '</p>' +
                '<p class="col-6">' +
                (results[i].types[prob].probability * 100).toFixed(1) + '%' +
                '</p>' +
                '</div>'
        }

        resultsWrapper.append(
            '<div class="col-md-4 pb-2">' +
            '<div class="card">' +
            '<div class="row container">' +
            '<div class="col-md-6">' +
            '<img class="card-img-top" src='+ results[i].feat_img_url+' alt="Card image cap">' +
            '</div>'+
            '<div class="col-md-6">' +
            '<img class="card-img-top" src='+ results[i].grad_img_url+' alt="Card image cap">' +
            '</div>'+
            '</div>'+
            '<div class="card-body">' +
            '<h5 class="card-title">' +
            'Файл ' + results[i].file_name +
            '</h5>' + probs +
            '</div>' +
            '</div>' +
            '</div>'
        );
    }
}