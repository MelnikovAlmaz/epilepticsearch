import pickle

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from epilepticapi.ml.grad_img_generator import get_feature_and_grad_images_paths
from .utils import fail_json_response, save_report
from epilepticapi.ml.prediction import get_prediction_for_file
from epilepticsearch import settings

import time

class CheckFilesView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckFilesView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        # Get files from form
        file_objects = request.FILES.getlist('document-files')
        # If file list is empty return error
        if not any(file_objects):
            return fail_json_response(error_code="NO_FILES_PRESENT")

        results = []
        for file_object in file_objects:
            file_result = {}
            file_result['file_name'] = file_object.name
            file_content = pickle.load(file_object.file)
            predictions = get_prediction_for_file(file_content, model_type='mean', use_cuda=False)
            feature_img_name, grad_img_name = get_feature_and_grad_images_paths(file_content, model_type='mean', use_cuda=False)

            feature_img_url = settings.MEDIA_URL + 'img/' + feature_img_name
            grad_img_url = settings.MEDIA_URL + 'img/' + grad_img_name

            types_predictions = {}
            for index, prediction in enumerate(predictions):
                type_prediction = {
                    "name": settings.LABEL_MAPPING_INV.get(index),
                    "probability": prediction
                }
                types_predictions["type_{}".format(index)] = type_prediction

            file_result['types'] = types_predictions
            file_result['class'] = predictions.index(max(predictions))
            file_result['feat_img_url'] = feature_img_url
            file_result['grad_img_url'] = grad_img_url
            results.append(file_result)

        # Generate report file
        report_file_name = save_report(results)
        report_file_url = settings.MEDIA_URL + 'reports/' + report_file_name

        response_result = {
            "success": True,
            "results": results,
            "report_url": report_file_url
        }
        return JsonResponse(data=response_result, safe=False)
