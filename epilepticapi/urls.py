from django.urls import path

from .views import CheckFilesView

app_name = 'api'
urlpatterns = [
    path('check_files', CheckFilesView.as_view(), name='check_files'),
]
