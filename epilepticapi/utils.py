from django.http import JsonResponse
import pandas as pd
import uuid
import os

from epilepticsearch import settings


def fail_json_response(error_code=""):
    response_result = {
        "success": False,
        "error_code": error_code
    }
    return JsonResponse(data=response_result, safe=False)


def save_report(report_data):
    report_df = pd.DataFrame(report_data)
    report_df = report_df[['file_name', 'class']]
    report_df['class'] = report_df['class'].map(settings.LABEL_MAPPING_INV)

    file_name = str(uuid.uuid1())
    file_path = os.path.join(settings.MEDIA_ROOT, 'reports/' + file_name + '.csv')

    report_df.to_csv(file_path, index=False)
    return file_name + '.csv'
