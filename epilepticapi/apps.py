from django.apps import AppConfig


class EpilepticapiConfig(AppConfig):
    name = 'epilepticapi'
