import torch.nn.functional as F

from epilepticapi.ml.ml_utils import load_signals_file, transform_signals, load_model
from epilepticsearch import settings

def get_prediction_for_file(file_name, model_type='mean', use_cuda=True):
    signals, label = load_signals_file(file_name)
    signals_tensor = transform_signals(signals, trans_type=model_type)

    model = load_model(model_type)
    if use_cuda:
        model.cuda()
        signals_tensor = signals_tensor.cuda()

    output = model(signals_tensor)
    output = F.softmax(model(signals_tensor)).detach().numpy().flatten().tolist()
    return output
