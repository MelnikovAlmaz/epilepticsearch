import numpy as np
import pickle
import torch
from torchvision import transforms

from epilepticsearch import settings


def load_signals_file(file):
    """
    Loads file with several signal shots.
    Each file contains 2 objects: list of signal shots, type of epilepsis (label)

    return: list of signal shots, label
    """
    signals = file.data
    label = file.seizure_type
    return signals, label


def mean_transform(signals):
    # take mean of samples
    mean_matrix = np.mean(signals, axis=0)
    mean_matrix = (mean_matrix - settings.BATCH_MEANS) / settings.BATCH_STD
    return mean_matrix


def mean_std_max_transform(signals):
    # take mean, std and max of samples
    _, num_channels, num_freq = signals.shape
    feature_matrix = np.zeros((num_channels, num_freq, 3))
    # Mean
    feature_matrix[:, :, 0] = np.mean(signals, axis=0)
    feature_matrix[:, :, 0] = (feature_matrix[:, :, 0] - settings.BATCH_MEANS) / settings.BATCH_STD
    # Std
    feature_matrix[:, :, 1] = np.std(signals, axis=0)
    # Max
    feature_matrix[:, :, 2] = np.max(signals, axis=0)
    feature_matrix[:, :, 2] = (feature_matrix[:, :, 2] - settings.BATCH_MEANS) / settings.BATCH_STD

    return feature_matrix


def transform_signals(signals, trans_type=None):
    transform_methods = {
        'mean': mean_transform,
        'mean_std_max': mean_std_max_transform
    }
    input_transforms = transforms.Compose([transforms.ToTensor(), ])

    transform_method = transform_methods.get(trans_type)
    if transform_method is not None:
        signals = transform_method(signals)

    tensor = input_transforms(signals)
    return torch.unsqueeze(tensor, 0)


def load_model(model_type='mean'):
    model_info_path = settings.MODEL_INFO_PATHS.get(model_type)
    model_info = torch.load(model_info_path, map_location='cpu')
    model_state = model_info.get('state_dict')
    input_shape = model_info.get('input_size')

    model = model_info.get('model')
    model.load_state_dict(model_state)

    return model
