"""
Django settings for epilepticsearch project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

import dill
import numpy as np

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'doiyy$w=c@6hxs@5r4x8%dv)d=$c@ewu&62r%)py!q75qphdy9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'epilepticapi',
    'frontend',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'epilepticsearch.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(os.path.join(BASE_DIR, 'frontend', 'templates')),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'epilepticsearch.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

#STATICFILES_DIRS = [
#    os.path.join(BASE_DIR, 'static'),
#]
STATIC_ROOT = os.path.join(BASE_DIR, "static")

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
IMAGES_ROOT = os.path.join(MEDIA_ROOT, 'img')

MEDIA_URL = "media/"

# ML
ML_FILES = os.path.join(BASE_DIR, "epilepticapi/ml/files")
with open(os.path.join(ML_FILES, 'batch_means.dill'), 'rb') as f:
    BATCH_MEANS = dill.load(f)
with open(os.path.join(ML_FILES, 'batch_std.dill'), 'rb') as f:
    BATCH_STD = dill.load(f)

LABEL_MAPPING = {
    'SPSZ': 3,
    'ABSZ': 0,
    'TCSZ': 4,
    'CPSZ': 2,
    'TNSZ': 5,
    'GNSZ': 1,
    'FNSZ': 6
}

LABEL_MAPPING_INV = {v:k for k,v in LABEL_MAPPING.items()}

LABEL_COUNT = len(LABEL_MAPPING.keys())

SAVED_MODELS_PATH = os.path.join(BASE_DIR, "epilepticapi/ml/saved_models")
MODEL_INFO_PATHS = {
        "mean": os.path.join(SAVED_MODELS_PATH, "input_mean_cv_models_best_0.9340.pth"),
        "mean_std_max": os.path.join(SAVED_MODELS_PATH, "softmax_cv_models_best_0.9270.pth")
    }
